## Installation

The setup process is very similar to this one [this one](https://drive.google.com/file/d/1qxUjd7LZGShw_a6bzunSnxPg9K6H9fM-/view?usp=sharing) you can follow the first steps to get setup if you don't have 
Python or git bash installed. 

In the folder you want to install the script: Right-click >> Show more options >> Git Bash here

```bash
git clone https://gitlab.com/antony.wavre/req_no_quotes_email_marketing.git
```
The project should now be installed. If not please refer to the [above video](https://drive.google.com/file/d/1qxUjd7LZGShw_a6bzunSnxPg9K6H9fM-/view?usp=sharing)

Click into the project folder then: Shift + Right-click >> Show more options >> Open PowerShell window here

Enter these commands one after the other (pressing Enter after each command)
```bash
pip install virtualenv
vitrutalenv venv
venv/scripts/activate
pip install -r requirements.txt
```
You might get an error:
```bash

× pip subprocess to install build dependencies did not run successfully.
│ exit code: 1
╰─> See above for output.

note: This error originates from a subprocess, and is likely not a problem with pip.
```
Make sure you have Microsoft Visual C++ 14.0 or greater.
Get it with "Microsoft C++ Build Tools": https://visualstudio.microsoft.com/visual-cpp-build-tools/
* Click "download build tools" wait for download
* Run the .exe
* Select the option on the top-left "Desktop development with C++" and install it